// console.log("Heeelloooo!~")


function Pokemon(name, level, health, attack){
	this.pokemonName = name;
	this.pokemonLevel = level;
	this.pokemonHealth =  2 * level;
	this.pokemonAttack = level;


	// tackle
	this.tackle = function(targetPokemon){

		console.log(this.pokemonName + "tackles " + targetPokemon.pokemonName)

		let hpAfterTackle = targetPokemon.pokemonHealth - this.pokemonAttack;

		console.log(targetPokemon.pokemonName + " health is now reduced to " + hpAfterTackle)

		targetPokemon.pokemonHealth = hpAfterTackle;
		console.log(targetPokemon);
		
		if(targetPokemon.pokemonHealth <= 0){
			targetPokemon.faint();
		}
	}

	// faint
	this.faint = function(){
				console.log(this.pokemonName + " fainted!")
	}	
	
}

let trainer = {
		name: "Ash Ketchum",
		age: 10,
		pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
		friends: {
				hoenn: ["May", "Max"],
				kanto: ["Brock","Misty"],
			},
		talk: function(){
			console.log("Pikachu! I choose you!")
			}
	}

console.log(trainer);

console.log("Result of Dot Notation:");
console.log(trainer.name);

console.log("Result of Square Bracket Notation:");
console.log(trainer["pokemon"]);

console.log("Result of Talk Method");
trainer.talk();


let pikachu = new Pokemon("Pikachu ", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude ", 8);
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo ", 100);
console.log(mewtwo);
	

geodude.tackle(pikachu);
mewtwo.tackle(geodude);

// geodude.faint();